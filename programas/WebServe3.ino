/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Estado de conexión
 */
#include "WiFi.h"

#define LED 2

const char * ssid="hercab2k20";
const char * passwd="AxVa9295";

WiFiServer server(80);  // Crear servidor que escucha peticiones por el puerto 80

String pagina="<!DOCTYPE html>"
              "<html>"
              "<head>"
              "<title>ESP32 1</title>"
              "</head>"
              "<body>"
              "<center><h1>Gesti&oacute;n de Led</h1>"
              "<hr/>"
              "<p><a href='/on'><button style='height:50px;width:100px'>ON</button></a></p>"
              "<p><a href='/off'><button style='height:50px;width:100px'>OFF</button></a></p>"
              "</center>"
              "</body>"
              "</html>";

void localIP(){
  Serial.print("IP asignada:");
  Serial.println(WiFi.localIP());
}

void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED){
        Serial.println("Conexión completada correctamente!!");
        localIP();
        server.begin();  // Si esta conectado inicia el servidor escuchando peticiones en el puerto indicado (80)
    }
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}


void setup()
{
    pinMode(LED, OUTPUT);
    Serial.begin(115200);
    delay(100);
    wifiInit();
    
}

int solicitudes=0;
String header="";
String linea="";
char c;

void loop()
{
    WiFiClient cliente= server.available(); // Revisa si existe alguna petición 
    
    if(cliente){  // Si existe la atiende y procesa.
      Serial.println("Nuevo cliente haciendo petición!!");
      Serial.print("Petición No.:");
      Serial.println(++solicitudes);
      while(cliente.connected()){
        if(cliente.available()){
          c= cliente.read();
          Serial.write(c);
          header+=c;
          if(c=='\n'){
            if(linea.length()==0){
              cliente.println("HTTP/1.1 200 OK");
              cliente.println("Content-type: text/html");
              cliente.println("Connection: close");
              cliente.println();
              
              if(header.indexOf("GET /off")>=0){
                digitalWrite(LED,LOW);
                Serial.println("Apaga el Led");
              }else if(header.indexOf("GET /on")>=0){
                digitalWrite(LED,HIGH);
                Serial.println("Enciende el led");
              }
              cliente.println(pagina);
              cliente.println();
              break;
            }else
               linea="";
               
          }else if(c!='\r')
              linea+=c;
        }
      }
      header="";
      cliente.stop();
      Serial.println("Respuesta enviada y cerrando conexión");
    }
}
