/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Estado de conexión
 */
#include "WiFi.h"


const char * ssid="hercab2k20";
const char * passwd="AxVa9295";

WiFiServer server(80);  // Crear servidor que escucha peticiones por el puerto 80

String pagina="<!DOCTYPE html>"
              "<html>"
              "<head>"
              "<title>ESP32 1</title>"
              "</head>"
              "<body>"
              "<center><h1>Hola Mundo</h1></center>"
              "</body>"
              "</html>";

void localIP(){
  Serial.print("IP asignada:");
  Serial.println(WiFi.localIP());
}

void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED){
        Serial.println("Conexión completada correctamente!!");
        localIP();
        server.begin();  // Si esta conectado inicia el servidor escuchando peticiones en el puerto indicado (80)
    }
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}



void setup()
{
    Serial.begin(115200);
    delay(100);
    wifiInit();
}

int solicitudes=0;
String linea="";

void loop()
{
    WiFiClient cliente= server.available(); // Revisa si existe alguna petición 
    
    if(cliente){  // Si existe la atiende y procesa.
      Serial.println("Nuevo cliente haciendo petición!!");
      Serial.print("Petición No.:");
      Serial.println(++solicitudes);
      while(cliente.connected()){
        if(cliente.available()){
          char c= cliente.read();
          Serial.write(c);
          if(c=='\n'){
            if(linea.length()==0){
              cliente.println("HTTP/1.1 200 OK");
              cliente.println("Content-type: text/html");
              cliente.println("Connection: close");
              cliente.println();
              cliente.println(pagina);
              cliente.println();
              break;
            }else
               linea="";
               
          }else if(c!='\r')
              linea+=c;
        }
      }
      cliente.stop();
      Serial.println("Respuesta enviada y cerrando conexión");
    }
}
